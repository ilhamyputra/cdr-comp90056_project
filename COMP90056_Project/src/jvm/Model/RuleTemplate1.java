package Model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.JsonObject;

public class RuleTemplate1 extends Rule implements Serializable{
	//if a land line makes more than X calls per month to international numbers, 
		//send a promotion for an international call package
		
		private TypeOfNumber typeOfcaller;
		private TypeOfNumber typeOfreciever;
		
		private String CountryOfCaller;		//International, local or all
		private String CountryOfReciever;
		
		private int numberOfCalls;
		private Promotion offeredPromotion;
		
		private int Duration; //Days
		private TimeUnit timeunit;
		
		public RuleTemplate1(){
			super.setRule_template("template1");
			super.setRule_description("if a land line makes more than X calls per month to international numbers, " +
										"send a promotion for an international call package");
			super.setActive(false);
			
			setTypeOfcaller(TypeOfNumber.Any);
			setTypeOfreciever(TypeOfNumber.Any);
			setCountryOfCaller("all");
			setCountryOfReciever("all");
			setNumberOfCalls(100);
			setOfferedPromotion(new Promotion());
			setDuration(30);
			setTimeunit(TimeUnit.Second);
			
			super.setRule_description(this.getRule_description());
		}
		
		public RuleTemplate1(JsonObject json){
			super.set_id(json.get("_id").getAsString());
			super.setRule_template(json.get("rule_template").getAsString());
			super.setRule_description(json.get("rule_description").getAsString());
			super.setActive(json.get("active").getAsBoolean());
			typeOfcaller = parseToEnumTypeOfNumber( json.get("typeOfcaller").getAsString() );
			typeOfreciever = parseToEnumTypeOfNumber( json.get("typeOfreciever").getAsString() );
			CountryOfCaller = json.get("CountryOfCaller").getAsString();
			CountryOfReciever = json.get("CountryOfReciever").getAsString();
			numberOfCalls = json.get("numberOfCalls").getAsInt();
			offeredPromotion = new Promotion(json.getAsJsonObject("offeredPromotion"));
			Duration = json.get("Duration").getAsInt();
			timeunit = parseToTimeUnit( json.get("TimeUnit").getAsString() );
			
			super.setRule_description(this.getRule_description());
		}
		
		
		public TypeOfNumber parseToEnumTypeOfNumber(String s){
			if(s.equalsIgnoreCase(TypeOfNumber.International.getType())){
				return TypeOfNumber.International;
			}
			else if(s.equalsIgnoreCase(TypeOfNumber.Local.getType())){
				return TypeOfNumber.Local;
			}
			else if(s.equalsIgnoreCase(TypeOfNumber.Any.getType())){
				return TypeOfNumber.Any;
			}
			
			return null;
		}
		
		
		public String getRuleTemplate(){
			return "select callingNumber, '" + getRule() + "' as rule, '"+ getPromotion() + "' as promotion, '"+ get_id() + "' as ruleId, count(callingNumber) as result from "+event+".win:time_batch(" + Duration + " " + timeunit.getType() + ") group by callingNumber, rule, promotion having count(callingNumber) > "+getNumberOfCalls();
		}
		
		public String getRule(){
			return  "if " + typeOfcaller.getType() + " users makes more than " + numberOfCalls + " calls per " + Duration + " " + timeunit.getType() + " to " + typeOfreciever.getType() + " numbers";
		}
		
		public String getPromotion(){
			return (int)(offeredPromotion.getDiscount()*100) + " discount for " + offeredPromotion.getCountry() + " call package for " + offeredPromotion.getPromotionDuration() + " days";
		}
		
		@Override
		public String getRule_description(){
			//if a land line makes more than X calls per month to international numbers, 
			//send a promotion for an international call package
			
			String description = "if " + typeOfcaller.getType() + " users makes more than <span class=\"label label-info\">" + numberOfCalls + " calls</span> per <span class=\"label label-info\">" +
								Duration + " " + timeunit.getType() + "</span> to <span class=\"label label-info\">" + typeOfreciever.getType() + "</span> numbers, send <span class=\"label label-info\">" + (int)(offeredPromotion.getDiscount()*100) 
								+ "%</span> discount for <span class=\"label label-info\">" + offeredPromotion.getCountry() + "</span> call package for <span class=\"label label-info\">" 
								+ offeredPromotion.getPromotionDuration() + " days</span>";
			
			return description;
		}

		public TypeOfNumber getTypeOfcaller() {
			return typeOfcaller;
		}

		public void setTypeOfcaller(TypeOfNumber typeOfcaller) {
			this.typeOfcaller = typeOfcaller;
		}

		public TypeOfNumber getTypeOfreciever() {
			return typeOfreciever;
		}

		public void setTypeOfreciever(TypeOfNumber typeOfreciever) {
			this.typeOfreciever = typeOfreciever;
		}

		public String getCountryOfCaller() {
			return CountryOfCaller;
		}

		public void setCountryOfCaller(String countryOfCaller) {
			CountryOfCaller = countryOfCaller;
		}

		public String getCountryOfReciever() {
			return CountryOfReciever;
		}

		public void setCountryOfReciever(String countryOfReciever) {
			CountryOfReciever = countryOfReciever;
		}

		public int getNumberOfCalls() {
			return numberOfCalls;
		}

		public void setNumberOfCalls(int numberOfCalls) {
			this.numberOfCalls = numberOfCalls;
		}

		public Promotion getOfferedPromotion() {
			return offeredPromotion;
		}

		public void setOfferedPromotion(Promotion offeredPromotion) {
			this.offeredPromotion = offeredPromotion;
		}

		public int getDuration() {
			return Duration;
		}

		public void setDuration(int Duration) {
			this.Duration = Duration;
		}
		
		public JsonObject getJsonObj(){
			JsonObject rule_obj = new JsonObject();
			JsonObject promotion_obj = offeredPromotion.getJsonObj();

			rule_obj.addProperty("rule_template", super.getRule_template());
			rule_obj.addProperty("rule_description", super.getRule_description());
			rule_obj.addProperty("active", super.isActive());
			rule_obj.addProperty("typeOfcaller", typeOfcaller.getType());
			rule_obj.addProperty("typeOfreciever", typeOfreciever.getType());
			rule_obj.addProperty("CountryOfCaller", CountryOfCaller);
			rule_obj.addProperty("CountryOfReciever", CountryOfReciever);
			rule_obj.addProperty("numberOfCalls", numberOfCalls);
			rule_obj.addProperty("Duration", getDuration());
			rule_obj.addProperty("TimeUnit", timeunit.getType());
			rule_obj.add("offeredPromotion", promotion_obj);
			
			
			return rule_obj;
		}

		public TimeUnit getTimeunit() {
			return timeunit;
		}

		public void setTimeunit(TimeUnit timeunit) {
			this.timeunit = timeunit;
		}
		
}
