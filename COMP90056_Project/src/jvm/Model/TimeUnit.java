package Model;

public enum TimeUnit {
	Year("year"), Month("month"), Day("day"), Hour("hour"), Minute("minute"), Second("second");
	
	private String type;
	
	private TimeUnit(String s){
		type = s;
	}
	
	public String getType() {
		return type;
	}
}
