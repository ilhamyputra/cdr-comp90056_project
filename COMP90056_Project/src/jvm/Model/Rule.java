package Model;

import java.io.Serializable;

public class Rule implements Serializable{
	
	private String _id;
	private String rule_template;
	private String rule_description;
	private boolean active;
	private TypeOfOperation operation;
	
	public static String event = "CallEvent";
	
	public String getRule_template() {
		return rule_template;
	}

	public void setRule_template(String rule_template) {
		this.rule_template = rule_template;
	}

	public String getRule_description() {
		return rule_description;
	}

	public void setRule_description(String rule_description) {
		this.rule_description = rule_description;
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public TypeOfOperation getOperation() {
		return operation;
	}

	public void setOperation(TypeOfOperation op) {
		operation = op;
	}
	
	public TimeUnit parseToTimeUnit(String s){
		if(s.equalsIgnoreCase(TimeUnit.Year.getType())){
			return TimeUnit.Year;
		}
		else if(s.equalsIgnoreCase(TimeUnit.Month.getType())){
			return TimeUnit.Month;
		}
		else if(s.equalsIgnoreCase(TimeUnit.Day.getType())){
			return TimeUnit.Day;
		}
		else if(s.equalsIgnoreCase(TimeUnit.Hour.getType())){
			return TimeUnit.Hour;
		}
		else if(s.equalsIgnoreCase(TimeUnit.Minute.getType())){
			return TimeUnit.Minute;
		}
		else if(s.equalsIgnoreCase(TimeUnit.Second.getType())){
			return TimeUnit.Second;
		}
		
		return null;
	}
	
}
