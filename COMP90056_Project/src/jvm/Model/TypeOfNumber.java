package Model;

public enum TypeOfNumber {
	Any("any"), Local("local"), International("international");
	
	private String type;
	
	private TypeOfNumber(String s){
		type = s;
	}
	
	public String getType() {
		return type;
	}
}
