package Model;

public enum TypeOfOperation {
	Add("add"), Del("del");
	
	private String type;
	
	private TypeOfOperation(String s){
		type = s;
	}
	
	public String getType() {
		return type;
	}
}
