package Model;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.google.gson.JsonObject;

public class Notification implements Serializable{
	
	private String _id;
	private long callingNumber;
	private String rule_id;
	private String rule;
	private String result;
	private String promotion;
	private Date recorded_date;
	
	private DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	
	public Notification(){
		callingNumber = 1231232323;
		rule_id = "xx";
		rule = "xx";
		result = "1";
		promotion = "xxxx";
		recorded_date = new Date();
	}

	public Notification(JsonObject json){
		
		callingNumber = json.get("callingNumber").getAsLong();
		rule_id = json.get("rule_id").getAsString();
		rule = json.get("rule").getAsString();
		result = json.get("result").getAsString();
		promotion = json.get("promotion").getAsString();
		
		try {
			recorded_date = dateFormat.parse( json.get("recorded_date").getAsString());
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	public Long getCallingNumber() {
		return callingNumber;
	}

	public void setCallingNumber(Long callingNumber) {
		this.callingNumber = callingNumber;
	}

	public String getRule_id() {
		return rule_id;
	}

	public void setRule_id(String rule_id) {
		this.rule_id = rule_id;
	}

	public String getRule() {
		return rule;
	}

	public void setRule(String rule) {
		this.rule = rule;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getPromotion() {
		return promotion;
	}

	public void setPromotion(String promotion) {
		this.promotion = promotion;
	}

	public Date getRecorded_date() {
		return recorded_date;
	}

	public void setRecorded_date(Date recorded_date) {
		this.recorded_date = recorded_date;
	}
	
	public JsonObject getJsonObj(){
		JsonObject rule_obj = new JsonObject();
		
		rule_obj.addProperty("callingNumber", callingNumber);
		rule_obj.addProperty("rule_id", rule_id);
		rule_obj.addProperty("rule", rule);
		rule_obj.addProperty("result", result);
		rule_obj.addProperty("promotion", promotion);
		rule_obj.addProperty("recorded_date", dateFormat.format(recorded_date));
		
		return rule_obj;
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}
	
}
