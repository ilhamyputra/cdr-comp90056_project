package Model;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.joda.time.DateTime;

public class CDR implements Serializable{
	
	private String cdrType;
	private String imsi;
	private String imei;
	private long callingNumber;
	private long calledNumber;
	private long recordingEntity;
	private int location;
	private long callReference;
	private double callDuration;
	private Date answerTime;
	private Date seizureTime;
	private Date releaseTime;
	private int causeForTermination;
	private int basicService;
	private long mscAddress;
	
	private TypeOfNumber typeOfCallingNumber;
	private TypeOfNumber typeOfCalledNumber;
	private String areaOfCallingNumber;
	private String areaOfCalledNumber;
	
	private Date timestamp;
	
	public CDR(String line){
		
		String[] temp = line.split(",");
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		this.setCdrType(temp[0]);
		setImsi(temp[1]);
		setImei(temp[2]);
		setCallingNumber(Long.parseLong(temp[3]));
		setCalledNumber(Long.parseLong(temp[4]));
		setRecordingEntity(Long.parseLong(temp[5]));
		setLocation(Integer.parseInt(temp[6]));
		setCallReference(Integer.parseInt(temp[7]));
		setCallDuration(Double.parseDouble(temp[8]));
		
		try {
			setAnswerTime(formatter.parse(temp[9]));
			setSeizureTime(null);
			setReleaseTime(formatter.parse(temp[11]));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		setCauseForTermination(Integer.parseInt(temp[12]));
		setBasicService(Integer.parseInt(temp[13]));
		setMscAddress(Long.parseLong(temp[14]));
	}

	public String getCdrType() {
		return cdrType;
	}

	public void setCdrType(String cdrType) {
		this.cdrType = cdrType;
	}

	public String getImsi() {
		return imsi;
	}

	public void setImsi(String imsi) {
		this.imsi = imsi;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public long getCallingNumber() {
		return callingNumber;
	}

	public void setCallingNumber(long callingNumber) {
		this.callingNumber = callingNumber;
	}

	public long getCalledNumber() {
		return calledNumber;
	}

	public void setCalledNumber(long calledNumber) {
		this.calledNumber = calledNumber;
	}

	public long getRecordingEntity() {
		return recordingEntity;
	}

	public void setRecordingEntity(long recordingEntity) {
		this.recordingEntity = recordingEntity;
	}

	public int getLocation() {
		return location;
	}

	public void setLocation(int location) {
		this.location = location;
	}

	public long getCallReference() {
		return callReference;
	}

	public void setCallReference(long callReference) {
		this.callReference = callReference;
	}

	public double getCallDuration() {
		return callDuration;
	}

	public void setCallDuration(double callDuration) {
		this.callDuration = callDuration;
	}

	public Date getAnswerTime() {
		return answerTime;
	}

	public void setAnswerTime(Date answerTime) {
		this.answerTime = answerTime;
	}

	public Date getSeizureTime() {
		return seizureTime;
	}

	public void setSeizureTime(Date seizureTime) {
		this.seizureTime = seizureTime;
	}

	public Date getReleaseTime() {
		return releaseTime;
	}

	public void setReleaseTime(Date releaseTime) {
		this.releaseTime = releaseTime;
	}

	public int getCauseForTermination() {
		return causeForTermination;
	}

	public void setCauseForTermination(int causeForTermination) {
		this.causeForTermination = causeForTermination;
	}

	public int getBasicService() {
		return basicService;
	}

	public void setBasicService(int basicService) {
		this.basicService = basicService;
	}

	public long getMscAddress() {
		return mscAddress;
	}

	public void setMscAddress(long mscAddress) {
		this.mscAddress = mscAddress;
	}
	
	public long getAreaCodeOfCallingNumber(){
		return (long)(callingNumber/10000000)%1000;
	}
	
	public long getAreaCodeOfCalledNumber(){
		return (long)(calledNumber/10000000)%1000;
	}

	public TypeOfNumber getTypeOfCallingNumber() {
		return typeOfCallingNumber;
	}

	public void setTypeOfCallingNumber(TypeOfNumber TypeOfCallingNumber) {
		this.typeOfCallingNumber = TypeOfCallingNumber;
	}

	public TypeOfNumber getTypeOfCalledNumber() {
		return typeOfCalledNumber;
	}

	public void setTypeOfCalledNumber(TypeOfNumber TypeOfCalledNumber) {
		this.typeOfCalledNumber = TypeOfCalledNumber;
	}

	public String getAreaOfCallingNumber() {
		return areaOfCallingNumber;
	}

	public void setAreaOfCallingNumber(String areaOfCallingNumber) {
		this.areaOfCallingNumber = areaOfCallingNumber;
	}

	public String getAreaOfCalledNumber() {
		return areaOfCalledNumber;
	}

	public void setAreaOfCalledNumber(String areaOfCalledNumber) {
		this.areaOfCalledNumber = areaOfCalledNumber;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	
	
}
