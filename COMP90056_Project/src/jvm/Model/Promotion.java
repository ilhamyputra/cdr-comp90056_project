package Model;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.Days;

import com.google.gson.JsonObject;

public class Promotion implements Serializable{
	
	private String country;	//International, Local, All
	private String type;
	private String description;
	private double discount;	//percentage
	private Date start_date;
	private Date end_date;
	
	private DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	
	public Promotion(){
		country = "all";	//International, Local, All
		type = "international";
		description = "Special discount offering cheaper international calls";
		start_date = new Date();
		DateTime temp = new DateTime(start_date);
		end_date = temp.plusDays(30).toDate();
		setDiscount(0.1);
	}
	
	public Promotion(JsonObject json){
		country = json.get("country").getAsString();
		type = json.get("type").getAsString();
		description = json.get("description").getAsString();
		discount = json.get("discount").getAsDouble();
		try {
			start_date = dateFormat.parse( json.get("start_date").getAsString() );
			end_date = dateFormat.parse( json.get("end_date").getAsString() );
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
	}
	
	public int getPromotionDuration(){
		DateTime start_date_new = new DateTime(start_date);
		DateTime end_date_new = new DateTime(end_date);
		return Days.daysBetween(start_date_new, end_date_new).getDays();
	}
	
	public void setPromotionDuration(int days){
		start_date = new Date();
		DateTime temp = new DateTime(start_date);
		end_date = temp.plusDays(days).toDate();
	}
	
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public Date getStart_date() {
		return start_date;
	}

	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}

	public Date getEnd_date() {
		return end_date;
	}

	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}
	
	public JsonObject getJsonObj(){
		JsonObject promotion_obj = new JsonObject();
		
		promotion_obj.addProperty("country", getCountry());
		promotion_obj.addProperty("type", getType());
		promotion_obj.addProperty("description", getDescription());
		promotion_obj.addProperty("discount", getDiscount());
		promotion_obj.addProperty("start_date", dateFormat.format(start_date));
		promotion_obj.addProperty("end_date", dateFormat.format(end_date));
		
		return promotion_obj;
	}
	
}
