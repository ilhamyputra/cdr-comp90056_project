package bolt;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import Model.CDR;
import Model.TypeOfNumber;
import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;


public class AreaClassificationBolt extends BaseBasicBolt {
	
	int count_null_area_calling = 0;
	int count_null_area_called = 0;
	int international_call = 0;
	int both_null = 0;
	int count_all = 0;
	
	Fields _outFields;
	String streamID;
	
	public AreaClassificationBolt(Fields fields, String streamID){
		this._outFields = fields;
		this.streamID = streamID;
	}

  @Override
  public void execute(Tuple tuple, BasicOutputCollector collector) {
	  CDR cdr = (CDR)tuple.getValue(0);
	  //System.out.println(cdr.getCdrType());
	  
	  long calledNum = cdr.getCalledNumber();
	  long callingNum = cdr.getCallingNumber();
	  
	  String calledNumArea = findAreaCodeFromFile("file_source/area_code.csv", calledNum);
	  String callingNumArea = findAreaCodeFromFile("file_source/area_code.csv", callingNum);
	  cdr.setAreaOfCalledNumber(calledNumArea);
	  cdr.setAreaOfCallingNumber(callingNumArea);
	  
	  //Set Enum type
	  if(calledNumArea != null){
		  cdr.setTypeOfCalledNumber(TypeOfNumber.Local);
	  }
	  else{
		  cdr.setTypeOfCalledNumber(TypeOfNumber.International);
	  }
	  
	  if(callingNumArea != null){
		  cdr.setTypeOfCallingNumber(TypeOfNumber.Local);
	  }
	  else{
		  cdr.setTypeOfCallingNumber(TypeOfNumber.International);
	  }
	  
	  collector.emit(streamID, new Values(cdr));
	  
	  /*System.out.println("-------");
	  System.out.println("calling Number(" + callingNum + ") = " + callingNumArea);
	  System.out.println("called Number(" + calledNum + ") = " + calledNumArea);*/
	  
	  if(callingNumArea == null){
		  count_null_area_calling++;
		  
	  }
	  if(calledNumArea == null){
		  count_null_area_called++;
	  }
	  if(callingNumArea == null && calledNumArea == null){
		  both_null++;
	  }
	  count_all++;
	  
	  /*System.out.println("The number of null area (calling Number)= " + count_null_area_calling);
	  System.out.println("The number of null area (called Number) = " + count_null_area_called);
	  System.out.println("The number of null (both) = " + both_null);
	  System.out.println("total number = " + count_all);*/
	  
  }

  
  private String findAreaCodeFromFile(String filename, long phone){

	  BufferedReader br = null;
	  long input_area_code = (long)(phone/10000000)%1000;
	  //System.out.println(input_area_code);
	  
		try {

			String sCurrentLine;

			br = new BufferedReader(new FileReader(filename));

			while ((sCurrentLine = br.readLine()) != null) {
				String[] fields = sCurrentLine.split(",");
				int area_code = Integer.parseInt(fields[0]);
				String area = fields[1];
				
				if(area_code == input_area_code){
					//System.out.println(input_area_code + " = " + area);
					return area;
				}
			
			}

		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			try {
				if (br != null){br.close();}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	  
	  return null;
  }

  @Override
  public void declareOutputFields(OutputFieldsDeclarer declarer) {
      declarer.declareStream(streamID, _outFields);
    }

}
