package bolt;

import Model.CDR;
import Model.Rule;
import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Tuple;

public class RulePrinterBolt extends BaseBasicBolt {
    
	
    @Override
    public void execute(Tuple tuple, BasicOutputCollector collector) {
    	Rule rule = (Rule)tuple.getValueByField("rule_template");
    	String id = tuple.getStringByField("rule_id");
    	
    	//System.out.println("Rule id:" + id + ", Operation:" + rule.getOperation().getType());
    	System.out.println("Rule id:" + id + ", Duration:" + rule.getRule_description());

    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {

    }

	
   
  }