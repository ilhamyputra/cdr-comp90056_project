package bolt;

import Model.CDR;
import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Tuple;

public class CDRPrinterBolt extends BaseBasicBolt {
    
	
    @Override
    public void execute(Tuple tuple, BasicOutputCollector collector) {
    	CDR cdr = (CDR)tuple.getValue(0);
    	
    	System.out.println("Calling Number: " + cdr.getCallingNumber() + ", Called Number: " + cdr.getCalledNumber());

    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {

    }

	
   
  }