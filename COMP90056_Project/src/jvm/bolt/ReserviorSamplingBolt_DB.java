package bolt;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.LinkedBlockingQueue;

import com.google.common.collect.Table;

import twitter4j.Status;
import DataSource.CouchDB;
import backtype.storm.Config;
import backtype.storm.Constants;
import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

public class ReserviorSamplingBolt_DB extends BaseBasicBolt {
    
	int period;	//in second
	List<Tuple> tuples;
	String streamID;
	Fields _outFields;
	int container_size;
	
	int counter;
	Random rand;
	
	public ReserviorSamplingBolt_DB(Fields fields, String streamID){
		CouchDB db = new CouchDB();

		this.period = db.getSamplingConfig_TimeWindow();
		this.container_size = db.getSamplingConfig_ContainerSize();
		this.streamID = streamID;
		this._outFields = fields;
		tuples = new ArrayList<Tuple>();
		this.counter = 0;
		rand = new Random();
	}
	
	private void updateConfig(){
		CouchDB db = new CouchDB();
		this.period = db.getSamplingConfig_TimeWindow();
		this.container_size = db.getSamplingConfig_ContainerSize();
	}
	
    @Override
    public void execute(Tuple tuple, BasicOutputCollector collector) {
    	if(isTickTuple(tuple)){
    		for(Tuple tuple1 : tuples){
    			collector.emit( streamID, new Values(tuple1.getValue(0)) );
    		}
    		tuples = new ArrayList<Tuple>();
    		updateConfig();
    	}
    	else{
    		//Sampling
    		buildSampledList(tuple);
    		//System.out.println(tuples.size());
    	}
     }
    
    private void buildSampledList(Tuple tuple){
    	//Add until the container is full
    	if( container_size > tuples.size() ){
    		tuples.add(tuple);
    	}
    	else if(counter > container_size){
    		double random = rand.nextDouble();
    		double probability = container_size/counter;
    		
    		if(random > probability){
    			int random_index = rand.nextInt(container_size);
    			tuples.remove(random_index);
    			tuples.add(random_index, tuple);
    		}
    	}
    	
    	counter++;
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
      declarer.declareStream(streamID, _outFields);
    }
    
    @Override
    public Map<String, Object> getComponentConfiguration() {
        Config conf = new Config();
        int tickFrequencyInSeconds = this.period;
        conf.put(Config.TOPOLOGY_TICK_TUPLE_FREQ_SECS, tickFrequencyInSeconds);
        return conf;
    }
    
    private static boolean isTickTuple(Tuple tuple) {
        return tuple.getSourceComponent().equals(Constants.SYSTEM_COMPONENT_ID)
            && tuple.getSourceStreamId().equals(Constants.SYSTEM_TICK_STREAM_ID);
    }
  }