package bolt;


import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;

public class TestBolt extends BaseBasicBolt {

	
    @Override
    public void execute(Tuple tuple, BasicOutputCollector collector) {
         System.out.println(tuple.getValue(0) + " " + tuple.getValue(1) + " " + tuple.getValue(2) + " " + tuple.getValue(3)+ " " + tuple.getValue(4));
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
      declarer.declare(new Fields("cdrphone","rules"));
    }

  }