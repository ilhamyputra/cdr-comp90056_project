package bolt;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.LinkedBlockingQueue;

import com.google.common.collect.Table;

import twitter4j.Status;
import DataSource.CouchDB;
import backtype.storm.Config;
import backtype.storm.Constants;
import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

public class DataRateMonitor_SinkToDB extends BaseBasicBolt {
	
	int period;
	int counter;
	String document_name;
	
	public DataRateMonitor_SinkToDB(int updateTime, String document_name){
		this.period = updateTime;
		counter = 0;
		this.document_name = document_name;
	}
	
    @Override
    public void execute(Tuple tuple, BasicOutputCollector collector) {
    	
    	if(isTickTuple(tuple)){
    		CouchDB db = new CouchDB();
    		System.out.println("Data rate = " + (double)counter/period + " tuple/s");
    		
    		if(document_name.equals("data_rate_original")){
    			db.updateDataRateOriginal((double)counter/period);
    		}
    		else if(document_name.equals("data_rate_after_sampling")){
    			db.updateDataRateAfterSampling((double)counter/period);
    		}
    		
    		counter = 0;	//Reset counter back to 0
    	}
    	else{
    		counter++;
    	}
     }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {

    }
    
    @Override
    public Map<String, Object> getComponentConfiguration() {
        Config conf = new Config();
        int tickFrequencyInSeconds = this.period;
        conf.put(Config.TOPOLOGY_TICK_TUPLE_FREQ_SECS, tickFrequencyInSeconds);
        return conf;
    }
    
    private static boolean isTickTuple(Tuple tuple) {
        return tuple.getSourceComponent().equals(Constants.SYSTEM_COMPONENT_ID)
            && tuple.getSourceStreamId().equals(Constants.SYSTEM_TICK_STREAM_ID);
    }
  }