package bolt;


import DataSource.CouchDB;
import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

public class CompactDB extends BaseBasicBolt {
	
	int resetNumber;
	int counter;
	
	public CompactDB(int resetNumber){
		this.resetNumber = resetNumber;
		counter = 0;
	}
	
    @Override
    public void execute(Tuple tuple, BasicOutputCollector collector) {
    	
    	if(counter >= resetNumber){
    		CouchDB db = new CouchDB();
    		db.compactStatusDB();
    		System.out.println(counter);
    		counter = 0;
    	}
    	
    	counter++;
     }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {

    }
    
  }