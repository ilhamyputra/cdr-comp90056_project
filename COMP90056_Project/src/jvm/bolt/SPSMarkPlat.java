package bolt;
import java.util.List;
import java.util.Map;

import Model.CDR;
import Model.Rule;
import Model.RuleTemplate1;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;


public class SPSMarkPlat extends BaseRichBolt {
	
	OutputCollector _collector;
	Fields cdrField;
	Fields ruleField;
	
	String cdrStreamID;
	String ruleStreamID;
	
	public SPSMarkPlat(Fields cdrField, Fields ruleField, String cdrStreamID, String ruleStreamID){
		this.cdrField = cdrField;
		this.ruleField = ruleField;
		this.cdrStreamID = cdrStreamID;
		this.ruleStreamID = ruleStreamID;
	}

	@Override
	public void prepare(Map stormConf, TopologyContext context,
		OutputCollector collector) {
		_collector = collector;
	}
	
	@Override
	public void execute(Tuple tuple) {
		
		if(tuple.getSourceGlobalStreamid().get_streamId().equals(cdrStreamID)){
			CDR cdr = (CDR)tuple.getValueByField("cdr");
			System.out.println(tuple.getSourceStreamId().toString() + ": Calling Number=" + cdr.getCallingNumber());
		}
		else if(tuple.getSourceGlobalStreamid().get_streamId().equals(ruleStreamID)){
			List<Rule> rules = (List<Rule>)tuple.getValueByField("rule");
			System.out.print(tuple.getSourceStreamId().toString() + ": ");
			
			for(int i=0;i<rules.size();i++){
				Rule rule = rules.get(i);
				System.out.println("	Rule" + (i+1) + ": " + rule.getRule_description());
			}
		}
		
		
		
		//List<Rule> rules = (List<Rule>)tuple.getValueByField("rule");
		
		/*if(cdr != null){
			System.out.println("------------");
			for(CDR temp : cdr){
				System.out.println("CDR = " + temp.getCdrType());
			}
			
		}*/
		
		/*if(rules != null){
			System.out.println("RULE = " + rules.get(0).getCountryOfCaller());
		}*/
		_collector.ack(tuple);
	}
	
	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		// TODO Auto-generated method stub
		
	}

}
