package bolt;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.LinkedBlockingQueue;

import com.google.common.collect.Table;

import twitter4j.Status;
import DataSource.CouchDB;
import Model.CDR;
import Model.TypeOfNumber;
import backtype.storm.Config;
import backtype.storm.Constants;
import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

public class StatReportBolt_DB extends BaseBasicBolt {
	
	int period;
	int counter;
	
	int international_call;
	int local_call;
	
	double percentage_inter;
	double percentage_local;
	
	public StatReportBolt_DB(int updateTime){
		this.period = updateTime;
		counter = 1;
		international_call = 0;
		local_call = 0;
		percentage_inter = 0.0;
		percentage_local = 0.0;
		
	}
	
    @Override
    public void execute(Tuple tuple, BasicOutputCollector collector) {
    	
    	if(isTickTuple(tuple)){
    		CouchDB db = new CouchDB();
    		percentage_inter = ((double)international_call/(double)counter);
    		percentage_local = ((double)local_call/(double)counter);
    		
    		System.out.println("International call = " + percentage_inter+ "%");
    		System.out.println("Local call = " + percentage_local + "%");
    		
    		db.updatePercentageInternationalCall(percentage_inter);
    		db.updatePercentageLocalCall(percentage_local);
    		
    		international_call = 0;
    		local_call = 0;
    		counter = 1;	//Reset counter back to 0
    	}
    	else{
    		CDR cdr = (CDR)tuple.getValue(0);
    		//System.out.println(cdr.getTypeOfCalledNumber().getType());
    		if(cdr.getTypeOfCalledNumber() == TypeOfNumber.International){
    			international_call++;
    		}
    		else if(cdr.getTypeOfCalledNumber() == TypeOfNumber.Local){
    			local_call++;
    		}
    		
    		counter++;
    	}
     }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {

    }
    
    @Override
    public Map<String, Object> getComponentConfiguration() {
        Config conf = new Config();
        int tickFrequencyInSeconds = this.period;
        conf.put(Config.TOPOLOGY_TICK_TUPLE_FREQ_SECS, tickFrequencyInSeconds);
        return conf;
    }
    
    private static boolean isTickTuple(Tuple tuple) {
        return tuple.getSourceComponent().equals(Constants.SYSTEM_COMPONENT_ID)
            && tuple.getSourceStreamId().equals(Constants.SYSTEM_TICK_STREAM_ID);
    }
  }