package bolt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.LinkedBlockingQueue;

import com.clearspring.analytics.stream.membership.BloomFilter;
import com.google.common.collect.Table;

import twitter4j.Status;
import Model.CDR;
import backtype.storm.Config;
import backtype.storm.Constants;
import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

public class DeduplicateBolt extends BaseBasicBolt {
    
	Fields out_fields;
	String out_streamID;
	double false_positive;
	int expectedsize;
	private static BloomFilter bloomFilter;
	
	public DeduplicateBolt(Fields fields, String streamID, double false_positive, int size){
		this.out_fields = fields;
		this.out_streamID = streamID;
		this.false_positive = false_positive;
		this.expectedsize = size;
		bloomFilter = new BloomFilter(size, false_positive);
	}
	
    @Override
    public void execute(Tuple tuple, BasicOutputCollector collector) {
    	
    CDR cdr = (CDR)tuple.getValue(0);
    String content =  cdr.getCallingNumber() + "" +
    				  cdr.getCalledNumber() + "" +
    				  cdr.getCallDuration();

    if(!bloomFilter.isPresent(content)){
    	//System.out.println(content);
    	bloomFilter.add(content);
    	collector.emit(out_streamID, new Values(cdr));
    }
    else{
    	//System.out.println(content + " Duplicated");    	
    }
    
    }

    

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
      declarer.declareStream(out_streamID, out_fields);
    }
    
  }