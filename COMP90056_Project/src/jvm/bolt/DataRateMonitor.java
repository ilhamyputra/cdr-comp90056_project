package bolt;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.LinkedBlockingQueue;

import com.google.common.collect.Table;

import twitter4j.Status;
import backtype.storm.Config;
import backtype.storm.Constants;
import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

public class DataRateMonitor extends BaseBasicBolt {
	
	int period;
	int counter;
	
	public DataRateMonitor(int updateTime){
		this.period = updateTime;
		counter = 0;
	}
	
    @Override
    public void execute(Tuple tuple, BasicOutputCollector collector) {
    	
    	if(isTickTuple(tuple)){
    		System.out.println("Data rate = " + (double)counter/period + " tuple/s");
    		counter = 0;	//Reset counter back to 0
    	}
    	else{
    		counter++;
    	}
     }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
    	
    }
    
    @Override
    public Map<String, Object> getComponentConfiguration() {
        Config conf = new Config();
        int tickFrequencyInSeconds = this.period;
        conf.put(Config.TOPOLOGY_TICK_TUPLE_FREQ_SECS, 0.1);
        return conf;
    }
    
    private static boolean isTickTuple(Tuple tuple) {
        return tuple.getSourceComponent().equals(Constants.SYSTEM_COMPONENT_ID)
            && tuple.getSourceStreamId().equals(Constants.SYSTEM_TICK_STREAM_ID);
    }
  }