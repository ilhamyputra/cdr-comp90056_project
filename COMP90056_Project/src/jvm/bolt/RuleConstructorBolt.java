package bolt;


import java.util.Map;

import Model.CDR;
import Model.Rule;
import Model.RuleTemplate1;
import Model.RuleTemplate2;
import Model.RuleTemplate3;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

public class RuleConstructorBolt extends BaseRichBolt {

	private static final long serialVersionUID = 1L;	
	public static String TEMPLATE_1 = "template1";
	public static String TEMPLATE_2 = "template2";
	public static String TEMPLATE_3 = "template3";
	
	OutputCollector _collector;

    
	@Override
	public void prepare(Map stormConf, TopologyContext context,
			OutputCollector collector) {
		_collector = collector;		
	}

	@Override
	public void execute(Tuple tuple) {
    	Rule rule = (Rule)tuple.getValue(0);   
    	if(rule.getRule_template().equals(TEMPLATE_1)){
    		RuleTemplate1 ruleTemplate = (RuleTemplate1)rule;
    	}
    	else if(rule.getRule_template().equals(TEMPLATE_2)){
    		RuleTemplate2 ruleTemplate = (RuleTemplate2)rule;
    	}
    	else if(rule.getRule_template().equals(TEMPLATE_3)){
    		RuleTemplate3 ruleTemplate = (RuleTemplate3)rule;
    	}
    	
  
	}

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
    	declarer.declare(new Fields("cdrType","callingNumber",
    								"calledNumber","location",
    								"callReference","callDuration",
    								"answerTime","seizureTime",
    								"releaseTime","causeForTermination",
    								"basicService","mscAddress",
    								"typeOfCallingNumber","typeOfCalledNumber",
    								"areaOfCallingNumber","areaOfCalledNumber"
    								));
    }

  }