package bolt;

import java.util.Map;

import Model.CDR;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

public class CDRConstructorBolt extends BaseRichBolt {
    
	private static final long serialVersionUID = 1L;
	OutputCollector _collector;
	String streamID;

	
	public CDRConstructorBolt(String streamID){
		this.streamID = streamID;
	}
	
	@Override
	public void prepare(Map stormConf, TopologyContext context,
			OutputCollector collector) {
		_collector = collector;		
	}

	@Override
	public void execute(Tuple tuple) {
    	CDR cdr = (CDR)tuple.getValue(0);    	
    	_collector.emit(streamID, 
    					new Values(cdr.getCallingNumber(), cdr.getCallDuration(),
    							   cdr.getCauseForTermination(), cdr.getTypeOfCallingNumber(), 
    							   "", "", cdr.getTimestamp()
    							   ));		
	}

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
    	declarer.declareStream(streamID,
    					 new Fields("callingNumber","callDuration",
    								"causeForTermination","typeOfCallingNumber",
    								"rule", "promotion", "Time"
    								));
    }
	
   
  }