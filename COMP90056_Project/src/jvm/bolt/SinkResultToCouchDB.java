package bolt;

import java.util.Date;

import DataSource.CouchDB;
import Model.Notification;
import Model.Rule;
import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Tuple;

public class SinkResultToCouchDB extends BaseBasicBolt {
    
	
    @Override
    public void execute(Tuple tuple, BasicOutputCollector collector) {
    	//"callingNumber","rule","promotion", "ruleId", "result"
    	CouchDB db = new CouchDB();
    	Notification notification = new Notification();
    	
    	notification.setCallingNumber( tuple.getLongByField("callingNumber") );
    	notification.setRule_id( tuple.getStringByField("ruleId") );
    	notification.setRule( tuple.getStringByField("rule") );
    	notification.setPromotion( tuple.getStringByField("promotion") );
    	notification.setResult( ""+tuple.getValueByField("result") );
    	
    	db.addNotification( notification );
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {

    }
	
	
}
