package bolt;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import com.google.common.collect.Table;

import twitter4j.Status;
import DataSource.CouchDB;
import backtype.storm.Config;
import backtype.storm.Constants;
import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

public class DataRateControllBolt extends BaseBasicBolt {
	
	int delay;	//in micosecond
	int polling_time_sec;
	String out_streamID;
	Fields out_fields;
	
	
	public DataRateControllBolt(Fields fields, String streamID, int rate){
		CouchDB db = new CouchDB();
		//delay = 1000000/rate;	//micro sec
		delay = rate;
		this.polling_time_sec = polling_time_sec;
		this.out_fields = fields;
		this.out_streamID = streamID;
	}
	
    @Override
    public void execute(Tuple tuple, BasicOutputCollector collector) {
    	
    	try {
    		collector.emit(out_streamID, new Values(tuple.getValue(0)));
    		//System.out.println("delay " + delay);

			TimeUnit.MICROSECONDS.sleep(delay);


		} catch (InterruptedException e) {
			e.printStackTrace();
				
		}
    	
    }
    

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
    	declarer.declareStream(out_streamID, out_fields);
    }
    
  }