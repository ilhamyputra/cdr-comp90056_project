package bolt;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

import DataSource.CouchDB;
import Model.Notification;
import Model.Rule;
import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Tuple;

public class SinkToFile extends BaseBasicBolt {
    
	String filename;
	
	public SinkToFile(String filename){
		this.filename = filename;
	}
	
    @Override
    public void execute(Tuple tuple, BasicOutputCollector collector) {
    	try {
    		 
			String content = "" + tuple.getValue(0) + "\n";
 
			File file = new File( filename );
 
			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}
 
			FileWriter fw = new FileWriter(file.getAbsoluteFile(),true);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(content);
			bw.close();
 
			//System.out.println("Done");
 
		} catch (IOException e) {
			e.printStackTrace();
		}

    	
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {

    }
	
	
}
