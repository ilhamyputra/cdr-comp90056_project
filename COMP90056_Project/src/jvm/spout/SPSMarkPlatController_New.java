package spout;
import DataSource.CouchDB;
import Model.*;
import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class SPSMarkPlatController_New extends BaseRichSpout {
	
	 SpoutOutputCollector _collector;
	 Fields _outFields;
	 List<Rule> current_rules;
	 List<Rule> prev_rules;
	 String streamID;
	 
	 public SPSMarkPlatController_New(Fields fields, String streamID){
		 _outFields = fields;	 
		 
		 current_rules = new ArrayList<Rule>();
		 this.streamID = streamID;
		 
		 current_rules = null;
		 prev_rules = null;
	 }

	  @Override
	  public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {
		  _collector = collector;
		  
	  }

	  @Override
	  public void nextTuple() {
		  
		try {
			CouchDB db = new CouchDB();
			current_rules = db.getAllRules();
			
			if(prev_rules == null){
				for(Rule rule : current_rules){
					if(rule.isActive()){
						rule.setOperation(TypeOfOperation.Add);
						_collector.emit(streamID, new Values(rule, rule.get_id()));
					}
				}
			}
			else{
				//Checking for rule deletion
				for(Rule prev_rule : prev_rules){
					boolean isMatch = false;
					
					for(Rule rule : current_rules){
						if( prev_rule.get_id().equals(rule.get_id()) ){
							isMatch = true;
							break;
						}
					}
					
					if(!isMatch && prev_rule.isActive()){
						prev_rule.setOperation(TypeOfOperation.Del);
						_collector.emit(streamID, new Values(prev_rule, prev_rule.get_id()));
					}
				}
				
				//Checking for new rule
				for(Rule rule : current_rules){
					boolean isMatch = false;
					
					for(Rule prev_rule : prev_rules){
						if( prev_rule.get_id().equals(rule.get_id()) ){
							isMatch = true;
							break;
						}
					}
					
					if(!isMatch && rule.isActive()){
						rule.setOperation(TypeOfOperation.Add);
						_collector.emit(streamID, new Values(rule, rule.get_id()));
					}
				}
				
				//Checking for active and inactive
				for(Rule rule : current_rules){
					for(Rule prev_rule : prev_rules){
						if( prev_rule.get_id().equals(rule.get_id()) &&
								prev_rule.isActive() != rule.isActive()){
							
							if(rule.isActive()){
								rule.setOperation(TypeOfOperation.Add);
								_collector.emit(streamID, new Values(rule, rule.get_id()));
							}
							else{
								rule.setOperation(TypeOfOperation.Del);
								_collector.emit(streamID, new Values(rule, rule.get_id()));
							}
							
						}
					}
				}
			}
			
			prev_rules = current_rules;
			
			//_collector.emit(streamID, new Values(current_rules));
			Thread.sleep(1000);
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	  }
	
	  @Override
	  public void ack(Object id) {
	  }
	
	  @Override
	  public void fail(Object id) {
	  }
	
	  @Override
	  public void declareOutputFields(OutputFieldsDeclarer declarer) {
		  //declarer.declare(new Fields("rules"));
		  //declarer.declare(_outFields);
		  declarer.declareStream(streamID, _outFields);
	  }

}