package spout;
import DataSource.CouchDB;
import Model.*;
import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class SPSMarkPlatController extends BaseRichSpout {
	
	 SpoutOutputCollector _collector;
	 Fields _outFields;
	 List<Rule> current_rules;
	 List<Rule> prev_rules;
	 String streamID;
	 int speed_rate;
	 
	 public SPSMarkPlatController(Fields fields, String streamID, int speed_rate){
		 _outFields = fields;
		 current_rules = new ArrayList<Rule>();
		 this.streamID = streamID;
		 this.speed_rate = speed_rate;
		 
		 CouchDB db = new CouchDB();
		 current_rules = db.getAllRules();
		 
		 /*RuleTemplate1 rule1 = new RuleTemplate1();
		 
		 rule1.setDurationDays(30); //One month
		  
		 RuleTemplate1 rule2 = new RuleTemplate1();
		 rule2.setDurationDays(7);	//One Week
		  
		 rules.add(rule1);
		 rules.add(rule2);*/
	 }

	  @Override
	  public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {
		  _collector = collector;
		  
	  }

	  @Override
	  public void nextTuple() {
		  
		try {
			_collector.emit(streamID, new Values(current_rules));
			if(speed_rate != 0){
				Thread.sleep(1/speed_rate);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	  }
	
	  @Override
	  public void ack(Object id) {
	  }
	
	  @Override
	  public void fail(Object id) {
	  }
	
	  @Override
	  public void declareOutputFields(OutputFieldsDeclarer declarer) {
		  //declarer.declare(new Fields("rules"));
		  //declarer.declare(_outFields);
		  declarer.declareStream(streamID, _outFields);
	  }

}