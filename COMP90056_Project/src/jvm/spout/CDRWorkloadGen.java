package spout;
import Model.CDR;
import backtype.storm.Config;
import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.Random;

/*
 * Data rate control
 */

public class CDRWorkloadGen extends BaseRichSpout {
	  SpoutOutputCollector _collector;
	  Random _rand;
	  Fields _outFields;
	  String streamID;
	  String filename;
	  
	  int rate;	// x * 100 tuple/s
	  static BufferedReader br;
	  int counter = 10;
	  
	  public CDRWorkloadGen(Fields fields, String streamID, int rate, String filename){
		  _outFields = fields;
		  this.streamID = streamID;
		  this.rate = rate/100;
		  this.filename = filename;
		  
		  try {
			br = new BufferedReader(new FileReader(filename));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	  }

	  @Override
	  public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {
		  _collector = collector;
		  _rand = new Random();
	  }

	  @Override
	  public void nextTuple() {	  
			try {
	 
				String sCurrentLine;
				if(counter+1 == 0){
					counter = rate;
				}
				else if((sCurrentLine = br.readLine()) != null) {

					CDR cdr = new CDR(sCurrentLine.replaceAll("\"", ""));
					cdr.setTimestamp(new Date());
					_collector.emit(streamID, new Values(cdr));
					
				}
				else{
					System.out.println("end"); 
					br.close();
				}
				
				counter--;

			} catch (IOException e) {
				e.printStackTrace();
			} 
	  }
	
	  @Override
	  public void ack(Object id) {
	  }
	
	  @Override
	  public void fail(Object id) {
	  }
	  
	  @Override
	    public Map<String, Object> getComponentConfiguration() {
	        Config conf = new Config();
	        conf.put(Config.TOPOLOGY_SLEEP_SPOUT_WAIT_STRATEGY_TIME_MS , 10);
	        return conf;
	    }
	
	  @Override
	  public void declareOutputFields(OutputFieldsDeclarer declarer) {
	    //declarer.declare(new Fields("cdr"));
		  //declarer.declare(_outFields);
		  //declarer.declareStream("cdr_stream", _outFields);
		  declarer.declareStream(streamID, _outFields);
	  }

}