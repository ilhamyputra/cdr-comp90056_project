package spout;
import DataSource.CouchDB;
import Model.CDR;
import backtype.storm.Config;
import backtype.storm.Constants;
import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class CDRWorkloadGen_Buffer_DB extends BaseRichSpout {
	  SpoutOutputCollector _collector;
	  Random _rand;
	  Fields _outFields;
	  String streamID;
	  String filename = "file_source/cdr.rawsample2.txt";
	  //String filename = "file_source/cdrfile.txt";
	  int delay;
	  int rate;
	  int period;
	  
	  int counter = 0;
	  int db_time;
	  
	  BufferedReader br = null;
	  
	  public CDRWorkloadGen_Buffer_DB(Fields fields, String streamID, int db_time, BufferedReader bufferreader){
		  CouchDB db = new CouchDB();
		  _outFields = fields;
		  this.streamID = streamID;
		  rate = db.getCdrDataRate();
		  this.delay = updateDelay(rate);
		  this.period = period; 
		  this.db_time = db_time;
	  }

	  @Override
	  public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {
		  _collector = collector;
		  _rand = new Random();
	  }
	  
	  private int updateDelay(int rate){
		  return 1000000/rate;
	  }

	  @Override
	  public void nextTuple() {
		  
		  
		  
			try {
	 
				String sCurrentLine;
	 
				if((sCurrentLine = br.readLine()) != null) {
					//System.out.println(sCurrentLine);
					CDR cdr = new CDR(sCurrentLine.replaceAll("\"", ""));
					cdr.setTimestamp(new Date());
					_collector.emit(streamID, new Values(cdr));
					TimeUnit.MICROSECONDS.sleep(delay);
					//Thread.sleep(delay);
					
					//Update delay every 1 second
					if(counter >= delay){
						CouchDB db = new CouchDB();
						rate = db.getCdrDataRate();
						delay = updateDelay(rate);
						counter = 0;
					}
					
					counter++;
				
				}

			} catch (IOException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	  }
	
	  @Override
	  public void ack(Object id) {
	  }
	
	  @Override
	  public void fail(Object id) {
	  }
	
	  @Override
	  public void declareOutputFields(OutputFieldsDeclarer declarer) {
		  declarer.declareStream(streamID, _outFields);
	  }
	  

}