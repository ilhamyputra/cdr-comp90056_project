package DataSource;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.lightcouch.CouchDbClient;
import org.lightcouch.CouchDbProperties;
import org.lightcouch.Response;

import com.google.gson.JsonObject;

import Model.Notification;
import Model.Rule;
import Model.RuleTemplate1;
import Model.RuleTemplate3;
import Model.RuleTemplate2;

public class CouchDB {
	CouchDbProperties properties;
	CouchDbClient dbClient;
	
	String rule_db = "cdr_rule_db";
	String status_db = "cdr_stream_status_db";
	String app_setting_db = "cdr_app_setting_db";
	String notification_db = "cdr_notification_db";

	public CouchDB(){
		/*properties = new CouchDbProperties()
		  .setCreateDbIfNotExist(true)
		  .setProtocol("http")
		  .setHost("127.0.0.1")
		  .setPort(5984)
		  .setUsername("admin")
		  .setPassword("z002d0wd")
		  .setMaxConnections(100)
		  .setConnectionTimeout(0);*/
			properties = new CouchDbProperties()
			  .setCreateDbIfNotExist(true)
			  .setProtocol("http")
			  .setHost("115.146.95.95")
			  .setPort(5984)
			  .setMaxConnections(100)
			  .setConnectionTimeout(0);
	}
	
	public CouchDB(CouchDbProperties properties){
		this.properties = properties;
	}
	
	private void selectRuleDB(){
		properties = properties.setDbName(rule_db);
		dbClient = new CouchDbClient(properties);
	}
	
	private void selectSettingDB(){
		properties = properties.setDbName(app_setting_db);
		dbClient = new CouchDbClient(properties);
	}
	
	private void selectStatusDB(){
		properties = properties.setDbName(status_db);
		dbClient = new CouchDbClient(properties);
	}
	
	private void selectNotificationDB(){
		properties = properties.setDbName(notification_db);
		dbClient = new CouchDbClient(properties);
	}
	
	public List<Rule> getAllRules(){
		selectRuleDB();
		
		List<Rule> temp = new ArrayList<Rule>();
		List<JsonObject> allRules = dbClient.view("rule/view_all_rules").query(JsonObject.class);
		
		for(JsonObject json : allRules){
			String template = json.getAsJsonObject("value").get("rule_template").getAsString();
			
			if(template.equals("template1")){
				temp.add(new RuleTemplate1(json.getAsJsonObject("value")));
			}
			else if(template.equals("template2")){
				temp.add(new RuleTemplate2(json.getAsJsonObject("value")));
			}
			else if(template.equals("template3")){
				temp.add(new RuleTemplate3(json.getAsJsonObject("value")));
			}
			
		}
		
		return temp;
	}
	
	public void addRule(Rule rule){
		selectRuleDB();
		
		Response response = null;
		
		if(rule.getRule_template().equals("template1")){
			RuleTemplate1 temp1 = (RuleTemplate1)rule;
			response = dbClient.save(temp1.getJsonObj());
		}
		else if(rule.getRule_template().equals("template2")){
			RuleTemplate2 temp2 = (RuleTemplate2)rule;
			response = dbClient.save(temp2.getJsonObj());
		}
		else if(rule.getRule_template().equals("template3")){
			RuleTemplate3 temp3 = (RuleTemplate3)rule;
			response = dbClient.save(temp3.getJsonObj());
		}
	}
	
	public void activateRule(String id, boolean active){
		selectRuleDB();
		
		JsonObject json = dbClient.find(JsonObject.class, id);
		json.addProperty("active", active);
		dbClient.update(json);
	}
	
	public void deleteRule(String id){
		selectRuleDB();
		
		JsonObject json = dbClient.find(JsonObject.class, id);
		dbClient.remove(json);
	}
	
	public boolean isTopologyActive(){
		selectSettingDB();
		
		JsonObject json = dbClient.find(JsonObject.class, "topology");
		return json.get("active").getAsBoolean();
	}
	
	public boolean isSamplingActive(){
		selectSettingDB();
		
		JsonObject json = dbClient.find(JsonObject.class, "sampling");
		return json.get("active").getAsBoolean();
	}
	
	public void activeSampling(boolean active){
		selectSettingDB();
		
		JsonObject json = dbClient.find(JsonObject.class, "sampling");
		json.addProperty("active", active);
		dbClient.update(json);
	}
	
	public int getCdrDataRate(){
		selectSettingDB();
		
		JsonObject json = dbClient.find(JsonObject.class, "cdr_data_rate");
		return json.get("rate").getAsInt();
	}
	
	public void setCdrDataRate(int rate){
		selectSettingDB();
		
		JsonObject json = dbClient.find(JsonObject.class, "cdr_data_rate");
		json.addProperty("rate", rate);
		dbClient.update(json);
	}
	
	public void updateDataRateOriginal(double rate){
		selectStatusDB();
		
		JsonObject json = dbClient.find(JsonObject.class, "data_rate_original");
		json.addProperty("rate", rate);
		dbClient.update(json);
	}
	
	public void updateDataRateAfterSampling(double rate){
		selectStatusDB();
		
		JsonObject json = dbClient.find(JsonObject.class, "data_rate_after_sampling");
		json.addProperty("rate", rate);
		dbClient.update(json);
	}
	
	public void updatePercentageLocalCall(double percentage){
		selectStatusDB();
		
		JsonObject json = dbClient.find(JsonObject.class, "percentage_local_call");
		json.addProperty("percentage", percentage);
		dbClient.update(json);
	}
	
	public void updatePercentageInternationalCall(double percentage){
		selectStatusDB();
		
		JsonObject json = dbClient.find(JsonObject.class, "percentage_international_call");
		json.addProperty("percentage", percentage);
		dbClient.update(json);
	}
	
	public double getPercentageLocalCall(){
		selectStatusDB();
		
		JsonObject json = dbClient.find(JsonObject.class, "percentage_local_call");
		return json.get("percentage").getAsDouble();
	}
	
	public double getPercentageInternationalCall(){
		selectStatusDB();
		
		JsonObject json = dbClient.find(JsonObject.class, "percentage_international_call");
		return json.get("percentage").getAsDouble();
	}
	
	public int getDataRateAfterSampling(){
		selectStatusDB();
		
		JsonObject json = dbClient.find(JsonObject.class, "data_rate_after_sampling");
		return( json.get("rate").getAsInt() );
	}
	
	public int getDataRateOriginal(){
		selectStatusDB();
		
		JsonObject json = dbClient.find(JsonObject.class, "data_rate_original");
		return( json.get("rate").getAsInt() );
	}
	
	public void compactStatusDB(){
		selectStatusDB();
		dbClient.context().compact();

	}
	
	public int getSamplingConfig_ContainerSize(){
		selectSettingDB();
		
		JsonObject json = dbClient.find(JsonObject.class, "sampling");
		return( json.get("container_size").getAsInt() );
	}
	
	public void setSamplingConfig_ContainerSize(int size){
		selectSettingDB();
		
		JsonObject json = dbClient.find(JsonObject.class, "sampling");
		json.addProperty("container_size", size);
		dbClient.update(json);
	}
	
	public int getSamplingConfig_TimeWindow(){
		selectSettingDB();
		
		JsonObject json = dbClient.find(JsonObject.class, "sampling");
		return( json.get("time_window").getAsInt() );
	}
	
	public void setSamplingConfig_TimeWindow(int second){
		selectSettingDB();
		
		JsonObject json = dbClient.find(JsonObject.class, "sampling");
		json.addProperty("time_window", second);
		dbClient.update(json);
	}
	
	public void setSamplingConfig_TumblingWindow(int number){
		selectSettingDB();
		
		JsonObject json = dbClient.find(JsonObject.class, "sampling");
		json.addProperty("tumbling_window", number);
		dbClient.update(json);
	}
	
	public void activeTopology(boolean active){
		selectSettingDB();
		
		JsonObject json = dbClient.find(JsonObject.class, "topology");
		json.addProperty("active", active);
		dbClient.update(json);
	}
	
	public int getSamplingConfig_TumblingWindowSize(){
		selectSettingDB();
		
		JsonObject json = dbClient.find(JsonObject.class, "sampling");
		return( json.get("tumbling_window").getAsInt() );
	}
	
	public void addNotification(Notification notification){
		selectNotificationDB();
		Response response = dbClient.save(notification.getJsonObj());
	}
	
	public List<Notification> getAllNotification(){
		selectNotificationDB();
		
		List<Notification> notifications = new ArrayList<Notification>();
		List<JsonObject> allNotifications = dbClient.view("time/view_notification")
												.descending(true)
												.query(JsonObject.class);
	
		for(JsonObject json : allNotifications){
			Notification n = new Notification(json.getAsJsonObject("value"));
			n.set_id( json.get("id").getAsString() );	//Set ID
			notifications.add( n );
		}
		
		return notifications;
	}
	
	public List<Notification> getNotifications(Date start_time, Date end_time){
		selectNotificationDB();
		
		DateTime s_time = new DateTime(start_time);
		DateTime e_time = new DateTime(start_time);
		
		List<Notification> notifications = new ArrayList<Notification>();
		List<JsonObject> allNotifications = dbClient.view("time/view_notification")
											.startKey(s_time.getYear() + ","  
														+ s_time.getMonthOfYear() + ","
														+ s_time.getDayOfMonth() + ","
														+ s_time.getHourOfDay() + ","
														+ s_time.getMinuteOfHour() + ","
														+ s_time.getSecondOfMinute())
											.startKey(e_time.getYear() + ","  
														+ e_time.getMonthOfYear() + ","
														+ e_time.getDayOfMonth() + ","
														+ e_time.getHourOfDay() + ","
														+ e_time.getMinuteOfHour() + ","
														+ e_time.getSecondOfMinute())
											.descending(true)
											.query(JsonObject.class);
		
		for(JsonObject json : allNotifications){
			Notification n = new Notification(json.getAsJsonObject("value"));
			n.set_id( json.get("id").getAsString() );	//Set ID
			notifications.add( n );
		}
		
		return notifications;
	}
	
	public void removeAllNotification(){
		selectNotificationDB();
		
		List<JsonObject> allNotifications = dbClient.view("time/view_notification")
												.query(JsonObject.class);
	
		for(JsonObject json : allNotifications){
			dbClient.remove(json.getAsJsonObject("value"));
		}
	}
	
	public static void main(String[] args){
		CouchDB db = new CouchDB();
		/*Notification n = new Notification();
		
		db.addNotification(n);*/
		db.updatePercentageInternationalCall(0.9);
		/*for(Notification n : db.getAllNotification()){
			System.out.println(n.getRecorded_date());
		}*/
		
	}
	
	
	
}
