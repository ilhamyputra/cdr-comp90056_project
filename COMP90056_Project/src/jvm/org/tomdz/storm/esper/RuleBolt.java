package org.tomdz.storm.esper;

import Model.Rule;
import Model.RuleTemplate1;
import Model.RuleTemplate2;
import Model.RuleTemplate3;
import Model.TypeOfOperation;
import backtype.storm.generated.GlobalStreamId;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

import com.espertech.esper.client.*;

import java.util.*;

public class RuleBolt extends BaseRichBolt implements UpdateListener
{
    private static final long serialVersionUID = 1L;
    
    static String cdrStreamID;
    static String ruleStreamID;
	public static String TEMPLATE_1 = "template1";
	public static String TEMPLATE_2 = "template2";
	public static String TEMPLATE_3 = "template3";
	int limit = 100;
	int counter = 0;
	int sum;

    public static class Builder
    {
        protected final RuleBolt bolt;

        public Builder(String cdrStrID, String ruleStrID)
        {
            this(new RuleBolt());
            cdrStreamID = cdrStrID;
            ruleStreamID = ruleStrID;
        }

        protected Builder(RuleBolt bolt)
        {
            this.bolt = bolt;
        }

        public InputsBuilder inputs()
        {
            return new InputsBuilder(bolt);
        }

        public OutputsBuilder outputs()
        {
            return new OutputsBuilder(bolt);
        }

        public StatementsBuilder statements()
        {
            return new StatementsBuilder(bolt);
        }

        public RuleBolt build()
        {
            return bolt;
        }
    }

    public static class InputsBuilder extends Builder
    {
        private InputsBuilder(RuleBolt bolt)
        {
            super(bolt);
        }

        public AliasedInputBuilder aliasComponent(String componentId)
        {
            return new AliasedInputBuilder(bolt, new StreamId(componentId));
        }

        public AliasedInputBuilder aliasStream(String componentId, String streamId)
        {
            return new AliasedInputBuilder(bolt, new StreamId(componentId, streamId));
        }
    }

    public static final class AliasedInputBuilder
    {
        private final RuleBolt bolt;
        private final StreamId streamId;
        private final Map<String, String> fieldTypes;

        private AliasedInputBuilder(RuleBolt bolt, StreamId streamId)
        {
            this(bolt, streamId, new HashMap<String, String>());
        }

        private AliasedInputBuilder(RuleBolt bolt, StreamId streamId, Map<String, String> fieldTypes)
        {
            this.bolt = bolt;
            this.streamId = streamId;
            this.fieldTypes = fieldTypes;
        }

        public TypedInputBuilder withField(String fieldNames)
        {
            return new TypedInputBuilder(bolt, streamId, fieldTypes, fieldNames);
        }

        public TypedInputBuilder withFields(String... fieldNames)
        {
            return new TypedInputBuilder(bolt, streamId, fieldTypes, fieldNames);
        }

        public InputsBuilder toEventType(String name)
        {
            bolt.addInputAlias(streamId, name, new TupleTypeDescriptor(fieldTypes));
            return new InputsBuilder(bolt);
        }
    }

    public static final class TypedInputBuilder
    {
        private final RuleBolt bolt;
        private final StreamId streamId;
        private final Map<String, String> fieldTypes;
        private final String[] fieldNames;

        private TypedInputBuilder(RuleBolt bolt, StreamId streamId, Map<String, String> fieldTypes, String... fieldNames)
        {
            this.bolt = bolt;
            this.streamId = streamId;
            this.fieldTypes = fieldTypes;
            this.fieldNames = fieldNames;
        }

        public AliasedInputBuilder ofType(Class<?> type)
        {
            for (String fieldName : fieldNames) {
                fieldTypes.put(fieldName, type.getName());
            }
            return new AliasedInputBuilder(bolt, streamId, fieldTypes);
        }
    }

    public static final class OutputsBuilder extends Builder
    {
        private OutputsBuilder(RuleBolt bolt)
        {
            super(bolt);
        }

        public OutputStreamBuilder onStream(String streamName)
        {
            return new OutputStreamBuilder(bolt, streamName);
        }

        public OutputStreamBuilder onDefaultStream()
        {
            return new OutputStreamBuilder(bolt, "default");
        }
    }

    public static final class OutputStreamBuilder
    {
        private final RuleBolt bolt;
        private final String streamName;

        private OutputStreamBuilder(RuleBolt bolt, String streamName)
        {
            this.bolt = bolt;
            this.streamName = streamName;
        }

        public NamedOutputStreamBuilder fromEventType(String name)
        {
            return new NamedOutputStreamBuilder(bolt, streamName, name);
        }

        public OutputsBuilder emit(String... fields)
        {
            bolt.setAnonymousOutput(streamName, fields);
            return new OutputsBuilder(bolt);
        }
    }

    public static final class NamedOutputStreamBuilder
    {
        private final RuleBolt bolt;
        private final String streamName;
        private final String eventTypeName;

        private NamedOutputStreamBuilder(RuleBolt bolt, String streamName, String eventTypeName)
        {
            this.bolt = bolt;
            this.streamName = streamName;
            this.eventTypeName = eventTypeName;
        }

        public OutputsBuilder emit(String... fields)
        {
            bolt.addNamedOutput(streamName, eventTypeName, fields);
            return new OutputsBuilder(bolt);
        }
    }

    public static final class StatementsBuilder extends Builder
    {
        private StatementsBuilder(RuleBolt bolt)
        {
            super(bolt);
        }

        public StatementsBuilder add(String statement)
        {
            bolt.addStatement(statement);
            return this;
        }
    }

    private final Map<StreamId, String> inputAliases = new LinkedHashMap<StreamId, String>();
    private final Map<StreamId, TupleTypeDescriptor> tupleTypes = new LinkedHashMap<StreamId, TupleTypeDescriptor>();
    private final Map<String, EventTypeDescriptor> eventTypes = new LinkedHashMap<String, EventTypeDescriptor>();
    private final List<String> statements = new ArrayList<String>();
    private transient EPServiceProvider esperSink;
    private transient EPRuntime runtime;
    private transient EPAdministrator admin;
    private transient OutputCollector collector;
    private HashMap<String,EPStatement> statementMap = new HashMap<String,EPStatement>();
    private Map<String, Object> properties = new HashMap<String, Object>();

    private RuleBolt()
    {
    }

    private void addInputAlias(StreamId streamId, String name, TupleTypeDescriptor typeDesc)
    {
        inputAliases.put(streamId, name);
        if (typeDesc != null) {
            tupleTypes.put(streamId, typeDesc);
        }
    }

    private void addNamedOutput(String streamId, String eventTypeName, String... fields)
    {
        eventTypes.put(eventTypeName, new EventTypeDescriptor(eventTypeName, fields, streamId));
    }

    private void setAnonymousOutput(String streamId, String... fields)
    {
        eventTypes.put(null, new EventTypeDescriptor(null, fields, streamId));
    }

    private void addStatement(String stmt)
    {
        statements.add(stmt);
    }

    public EventTypeDescriptor getEventType(String name)
    {
        return eventTypes.get(name);
    }

    public Collection<EventTypeDescriptor> getEventTypes()
    {
        return new ArrayList<EventTypeDescriptor>(eventTypes.values());
    }

    public EventTypeDescriptor getEventTypeForStreamId(String streamId)
    {
        for (EventTypeDescriptor eventType: eventTypes.values()) {
            if (streamId.equals(eventType.getStreamId())) {
                return eventType;
            }
        }
        return null;
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer)
    {
        for (EventTypeDescriptor eventType: eventTypes.values()) {
            declarer.declareStream(eventType.getStreamId(), eventType.getFields());
        }
        //--------For Testing----------
        declarer.declareStream("sink_data_rate",new Fields("rate"));
      //--------For Testing----------
    }

    @Override
    public void prepare(@SuppressWarnings("rawtypes") Map conf,
                        TopologyContext context,
                        OutputCollector collector)
    {
        this.collector = collector;

        Configuration configuration = new Configuration();

        setupEventTypes(context, configuration);

        this.esperSink = EPServiceProviderManager.getProvider(this.toString(), configuration);
        this.esperSink.initialize();
        this.runtime = esperSink.getEPRuntime();
        this.admin = esperSink.getEPAdministrator();
    }

   /* private String getEventTypeName(String componentId, String streamId)
    {
        String alias = inputAliases.get(new StreamId(componentId, streamId));

        if (alias == null) {
            alias = String.format("%s_%s", componentId, streamId);
        }
        return alias;
    }*/

    private void setupEventTypes(TopologyContext context, Configuration configuration)
    {
        Set<GlobalStreamId> sourceIds = context.getThisSources().keySet();

        for (GlobalStreamId id : sourceIds) {       	
            //String eventTypeName = getEventTypeName(id.get_componentId(), id.get_streamId());
        	if(id.get_streamId() != ruleStreamID){
                Fields fields = context.getComponentOutputFields(id.get_componentId(), id.get_streamId());
                TupleTypeDescriptor typeDesc = tupleTypes.get(new StreamId(id.get_componentId(), id.get_streamId()));
                setupEventTypeProperties(fields, typeDesc);     
        	}       
        }
        configuration.addEventType(Rule.event, properties);
    }

    private void setupEventTypeProperties(Fields fields, TupleTypeDescriptor typeDesc)
    {
        int numFields = fields.size();

        for (int idx = 0; idx < numFields; idx++) {
            String fieldName = fields.get(idx);
            Class<?> clazz = null;

            if (typeDesc != null) {
                String clazzName = typeDesc.getFieldType(fieldName);

                if (clazzName != null) {
                    try {
                        clazz = Class.forName(clazzName);
                    }
                    catch (ClassNotFoundException ex) {
                        throw new RuntimeException("Cannot find class " + clazzName + "declared for field " + fieldName);
                    }
                }
            }
            if (clazz == null) {
                clazz = Object.class;
            }
            if(fieldName.equals("callDuration")){
            	properties.put(fieldName, Double.class);
            }
            else if(fieldName.equals("causeForTermination")){
            	properties.put(fieldName, Integer.class);
            }
            else{
            	properties.put(fieldName, clazz);
            }
        }
    }

    @Override
    public void execute(Tuple tuple)
    {
    	//System.out.println(tuple.getValueByField("callingNumber"));
		if(tuple.getSourceGlobalStreamid().get_streamId().equals(ruleStreamID)){
			Rule rule = (Rule)tuple.getValueByField("rule_template"); 
			String ruleTemplate = null;
	    	if(rule.getRule_template().equals(TEMPLATE_1)){
	    		ruleTemplate = ((RuleTemplate1)rule).getRuleTemplate();
	    	}
	    	else if(rule.getRule_template().equals(TEMPLATE_2)){
	    		ruleTemplate = ((RuleTemplate2)rule).getRuleTemplate();
	    	}
	    	else if(rule.getRule_template().equals(TEMPLATE_3)){
	    		ruleTemplate = ((RuleTemplate3)rule).getRuleTemplate();
	    	}
	    	
			if(rule.getOperation() == TypeOfOperation.Add){
		    	if(statementMap.get(ruleTemplate) == null){
		    		System.out.println(ruleTemplate);
		    		EPStatement statement = admin.createEPL(ruleTemplate);
		            statement.addListener(this);
		            statementMap.put(ruleTemplate, statement);
		    	}
			}
			else if(rule.getOperation() == TypeOfOperation.Del){
		    	if(statementMap.get(ruleTemplate) != null){
		    		statementMap.get(ruleTemplate).removeListener(this);
		    		statementMap.get(ruleTemplate).destroy();
		    		statementMap.remove(ruleTemplate);
		    	}
			}
		}
        /*if(!x.equals("select rule as tps, cdr as maxRetweets from SPlat.win:time_batch(5 sec)")){
            x = "select rule as tps, cdr as maxRetweets from SPlat.win:time_batch(5 sec)";
            statementMap.get(x).removeListener(this);
            statementMap.remove(x);
        }*/

		else if(tuple.getSourceGlobalStreamid().get_streamId().equals(cdrStreamID)){
	        
			//String eventType = getEventTypeName(tuple.getSourceComponent(), tuple.getSourceStreamId());
	        Map<String, Object> data = new HashMap<String, Object>();
	        Fields fields = tuple.getFields();
	        int numFields = fields.size();
	
	        for (int idx = 0; idx < numFields; idx++) {
	            String name = fields.get(idx);
	            Object value = tuple.getValue(idx);
	
	            data.put(name, value);
	        }
	       // System.out.println("eventtype = "+eventType);
	        runtime.sendEvent(data, Rule.event);
	        
	        long startime = ((Date)tuple.getValueByField("Time")).getTime();
	        long endtime = new Date().getTime();
	        
	        sum = sum + (int)endtime - (int)startime;
	        counter++;
	        if(counter == limit){
	        	//System.out.println("Elapsed milliseconds: " + (double)sum/counter);
	        	 collector.emit("sink_data_rate", new Values((double)sum/counter));
	        	counter = 0;
	        	sum = 0;
	        }
	        
	        
	        //-----For Testing
	        //collector.emit("testing", new Values(tuple.getValue(0)));
	      //-----For Testing
	        collector.ack(tuple);	
		}
    }

    @Override
    public void update(EventBean[] newEvents, EventBean[] oldEvents)
    {
        if (newEvents != null) {
            for (EventBean newEvent : newEvents) {
                EventTypeDescriptor eventType = getEventType(newEvent.getEventType().getName());

                if (eventType == null) {
                    // anonymous event ?
                    eventType = getEventType(null);
                }
                if (eventType != null) {
                    collector.emit(eventType.getStreamId(), toTuple(newEvent, eventType.getFields()));
                }
            }
        }
    }

    private List<Object> toTuple(EventBean event, Fields fields)
    {
        int numFields = fields.size();
        List<Object> tuple = new ArrayList<Object>(numFields);

        for (int idx = 0; idx < numFields; idx++) {
            tuple.add(event.get(fields.get(idx)));
        }
        return tuple;
    }
    
    @Override
    public void cleanup()
    {
        if (esperSink != null) {
            esperSink.destroy();
        }
    }
}
