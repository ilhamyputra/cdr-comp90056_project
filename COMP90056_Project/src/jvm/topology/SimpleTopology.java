package topology;
import java.io.BufferedReader;
import java.io.FileReader;

import org.tomdz.storm.esper.RuleBolt;

import DataSource.CouchDB;
import Model.Rule;
import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.StormSubmitter;
import backtype.storm.drpc.LinearDRPCTopologyBuilder;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.tuple.Fields;
import bolt.AreaClassificationBolt;
import bolt.CDRConstructorBolt;
import bolt.CDRPrinterBolt;
import bolt.CompactDB;
import bolt.DataRateControllBolt;
import bolt.DataRateMonitor;
import bolt.DataRateMonitor_SinkToDB;
import bolt.DeduplicateBolt;
import bolt.DeduplicateBolt;
import bolt.ReserviorSamplingBolt;
import bolt.ReserviorSamplingBolt_DB;
import bolt.ReserviorSamplingTW_DB;
import bolt.RulePrinterBolt;
import bolt.SinkResultToCouchDB;
import bolt.SinkToFile;
import bolt.StatReportBolt_DB;
import bolt.TestBolt;
import spout.CDRWorkloadGen;
import spout.CDRWorkloadGen_DB;
import spout.CDRWorkloadGen;
import spout.SPSMarkPlatController;
import spout.SPSMarkPlatController_New;

/**
 * This topology demonstrates Storm's stream groupings and multilang capabilities.
 */
public class SimpleTopology {

  public static void main(String[] args) throws Exception {

	  //For testing
	  int number_of_workers = 20;
	  int number_of_evaluator = 10;
	  int data_rate = 20;	// X * 100 t/s
	  //String testing_file_name = "workload_testing" + number_of_evaluator + "evaluator/" + data_rate + "00tps.csv";
	  String testing_file_name = "workload_testing" + number_of_evaluator + "evaluator/" + data_rate + "00tps" + number_of_workers + "workers.csv";
	  
	  String cdr_filename = "file_source/cdr.rawsample2.txt";
	  System.setProperty("org.apache.commons.logging.Log",
              "org.apache.commons.logging.impl.NoOpLog");
	  
	
    TopologyBuilder builder = new TopologyBuilder();
    Fields cdrField = new Fields("cdr");	//Field of stream 1 from CDRWorkloadGen
    Fields ruleField = new Fields("rule_template","rule_id");	//Field of stream 1 from CDRWorkloadGen
    
    String cdrStreamID = "cdr_stream";
    String ruleStreamID = "rule_stream";

    builder.setSpout("Rulespout", new SPSMarkPlatController_New(ruleField, ruleStreamID));
    
    //builder.setSpout("CDRspout", new CDRWorkloadGen_DB(cdrField, "originalCDR"));
    //builder.setSpout("CDRspout", new CDRWorkloadGen_DB(cdrField, "originalCDR",10000));
    
    //builder.setSpout("CDRspout", new FileSource(cdrField, "originalCDR"), 3);
    //builder.setSpout("CDRspout", new CDRWorkloadGen_DB(cdrField, "originalCDR",2500));
    builder.setSpout("CDRspout", new CDRWorkloadGen(cdrField, "originalCDR", 100, cdr_filename ));
    //builder.setBolt("RateController", new DataRateControllBolt(cdrField, "adjustedStream", 1000),3)
	//							.shuffleGrouping("CDRspout", "originalCDR");
    
    builder.setBolt("RateMonitor1", new DataRateMonitor(1))
								.shuffleGrouping("CDRspout", "originalCDR");
    
    /*builder.setBolt("RateMonitor1", new DataRateMonitor_SinkToDB(1,"data_rate_original"))
	.shuffleGrouping("CDRspout", "originalCDR");*/
    
    /*builder.setBolt("Sampling", new ReserviorSamplingBolt(600, 1, cdrField, "SamplingStream"))
    							.shuffleGrouping("CDRspout", "originalCDR");
    
    builder.setBolt("RateMonitor2", new DataRateMonitor(1))
								.shuffleGrouping("Sampling", "SamplingStream");*/
    
    //builder.setBolt("CompactDB", new CompactDB(1000))
	//									.shuffleGrouping("CDRspout", "originalCDR");
    
    builder.setBolt("Deduplicate", new DeduplicateBolt(cdrField, "non-duplicated", 0.01, 1000))
    									.shuffleGrouping("CDRspout", "originalCDR");
    
    /*builder.setBolt("AreaClassification", new AreaClassificationBolt(cdrField, "classifiedStream"),5)
								.shuffleGrouping("Deduplicate", "non-duplicated");
    
    //builder.setBolt("Stat", new StatReportBolt_DB(1))
	//							.shuffleGrouping("AreaClassification", "classifiedStream");
    
    builder.setBolt("CDRConstructor", new CDRConstructorBolt(cdrStreamID), 10)
								.shuffleGrouping("AreaClassification", "classifiedStream");
    
    RuleBolt bolt = new RuleBolt.Builder(cdrStreamID,ruleStreamID)
    .inputs().aliasComponent("rules").toEventType(Rule.event)
    .outputs().onDefaultStream().emit("callingNumber","rule","promotion", "ruleId", "result")
    .build();
    
    builder.setBolt("rulebolt", bolt, number_of_evaluator)
    .fieldsGrouping("Rulespout", ruleStreamID, new Fields("rule_id"))
    //.shuffleGrouping("CDRConstructor", cdrStreamID);
    .allGrouping("CDRConstructor", cdrStreamID);
    
    builder.setBolt("SinkDataRateToFile", new SinkToFile(testing_file_name))
    				.shuffleGrouping("rulebolt", "sink_data_rate");*/
    
    //builder.setBolt("sinkToFile", new SinkResultToCouchDB()).shuffleGrouping("rulebolt");
    //builder.setBolt("test", new TestBolt()).shuffleGrouping("rulebolt");

    Config conf = new Config();
    conf.setDebug(false);

    if (args != null && args.length > 0) {
      conf.setNumWorkers(1);

      StormSubmitter.submitTopology(args[0], conf, builder.createTopology());

    }
    else {
      conf.setMaxTaskParallelism(number_of_workers);
      conf.setMaxSpoutPending(100);
      //conf.setNumWorkers(number_of_workers);

      LocalCluster cluster = new LocalCluster();
      cluster.submitTopology("generator", conf, builder.createTopology());
      
      Thread.sleep(5 * 60 * 1000);
      
      cluster.killTopology("generator");
      cluster.shutdown();
    }
  }
}